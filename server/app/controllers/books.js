/*jslint node: true */
"use strict";
/**
 * TODO: Define variables
 */
var mongoose = require('mongoose'),
    Book = mongoose.model('Book');

/**
 * CREATE a book
 * --------------------
 * Controller to create a book.
 *
 * Instructions, hints and questions
 * - Read about the 'save' method from Mongoose.
 * - Use the 'save' method from Mongoose.
*   - Question: What are the differences between MongoDb and Mongoose?
*   - Awnser: MongoDB is a database system, Mongoose is a framework that uses mapping between objects.
*   - Question: explain the concepts of 'schema type' and 'model'. How are they related?
*   - Awnser: A Schema is the setup for a document, a model is the actual document.
* - Return all fields.
* - Use the model "Book".
*
* The return object has three properties:
    *
* - meta: These are all optional and free to extend
*   - method name: The name of the method
*   - timestamp
*   - filename: The name of the file. Use '__filename' for this.
                                                           *   - duration: Duration of execution, time spend on server or other meaningful metric
* - doc: The result object, in case of retrieving all objects, this is always an array. No documents is returned as an empty array.
* - err: If no errors, it has the value 'null'
*
* Errors are not thrown in the node application but returned to the user.
* - Question: What will happen if you throw an error on the server?
* - Awnser: The server stops.
* @param req
* @param res
* @see http://docs.mongodb.org/manual/reference/method/db.collection.save/
    * @see http://mongoosejs.com/docs/api.html#model_Model-save
    * @module books/create
*/
/**
 * TODO: Create a CREATE document controller
 */

exports.create = function (req, res) {
    var doc = new Book(req.body);

    doc.save(function (err) {

        var retObj = {
            meta: {"action": "create",
                'timestamp': new Date(),
                filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    });
};

/**
 * RETRIEVE _all_ books
 * --------------------
 * Controller to retrieve _all_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'find' method from Mongoose.
 * - Use the 'save' method from Mongoose.
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   - Awnser: MongoDB is a database system, Mongoose is a framework that uses mapping between objects.
 * - The 'query' parameter is an empty object.
 *   - Question: Why is it empty?
 *   - Answer: Sending a empty object will ensure the return of all objects.
 * - Skip the options.
 *   - Question: Describe the options.
 *   - Answer: No options.
 * - Return all fields.
 * - Use the model "Book".
 *
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object, in case of retrieving all objects, this is always an array. No documents is returned as an empty array.
 * - err: If no errors, it has the value 'null'
 *
 * Errors are not thrown in the node application but returned to the user.
 * - Question: What will happen if you throw an error on the server?
 * - Answer: The server stops.
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.find/
 * @see http://mongoosejs.com/docs/api.html#model_Model.find
 * @module books/list
 */
/**
 * TODO: Create a RETRIEVE all document controller
 */

exports.list = function (req, res) {
    var conditions, fields, sort;

    conditions = {};
    fields = {};
    sort = {modificationDate: -1};

    console.log('list');

    Book
        .find(conditions, fields)
        .sort(sort)
        .exec(function (err, doc) {
            var retObj = {
                meta: {"action": "list",
                    'timestamp': new Date(),
                    filename: __filename},
                doc: doc,
                err: err
            };

            return res.send(retObj);
        });
};

/**
 * RETRIEVE _one_ book
 * --------------------
 * Controller to retrieve _one_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'findOne' method from Mongoose.
 * - Use the 'findOne' method from Mongoose.
 *   - Question: What is de result object from findOne?
 *   - Answer: One book.
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   - Awnser: MongoDB is a database system, Mongoose is a framework that uses mapping between objects.
 * - The 'query' parameter is an empty object.
 *   - Question: Why is it empty?
 *   - Answer: There is a parameter, this is used to find the correct document.
 * - Skip the options.
 * - Return all fields.
 * - Use the model "Book".
 * Question: Define route parameters and body parameter. What are the differences?
 * - Answer: Route parameters add additional info to the page: /books/'id'
 *           Body parameters tell you which page it is /books/'genre'
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object is either an object or null.
 * - err: If no errors, it has the value 'null'
 *
 * @module books/detail
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.findOne/
 * @see http://mongoosejs.com/docs/api.html#model_Model.findOne
 */
/**
 * TODO: Create a RETRIEVE 1 document controller
 */

exports.detail = function (req, res) {
    var conditions, fields;
    conditions = { _id: req.params._id };
    fields = {};

    Book
        .findOne(conditions, fields)
        .exec(function (err, doc) {
            var retObj = {
                meta: { "action": "detail", "timestamp": new Date(), filename: __filename },
                doc: doc,
                err: err
            };
            return res.send(retObj);
        });
};

/**
 * UPDATE book
 * --------------------
 * Controller to update _one_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'find' method from Mongoose.
 * - Use the 'findOneAndUpdate' method from Mongoose.
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   Answer: MongoDb is database system . Mongoose is an object-relational mapping framework.
 *   - Question: What are the differences between MongoDb 'save' and MongoDb 'update'?
 *   - Answer: Save creates a new document, Update changes an already saved document.
 * - Return all fields.
 * - Use the model "Book".
 * Question: What changes should be made to update more than one document?
 *
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object is either an object or null.
 * - err: If no errors, it has the value 'null'
 *
 * @module books/update
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.update/
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.save/
 * @see http://mongoosejs.com/docs/api.html#model_Model.findOneAndUpdate
 */
/**
 * TODO: Create a UPDATE document controller
 */

exports.updateOne = function (req, res) {
    var conditions =
        {_id: req.params._id},
        update = {
            title: req.body.title || '',
            author: req.body.author || '',
            description: req.body.description || ''
        },
        options = {multi: false},
        callback = function (err, doc) {
            var retObj = {
                meta: {"action": "update",
                    'timestamp': new Date(),
                    filename: __filename
                    },
                doc: doc,
                err: err
            };
            return retObj;
        };
    Book.findOneAndUpdate(conditions, update, options, callback);
};

/**
 * DELETE
 * remove @ http://mongoosejs.com/docs/api.html#model_Model-remove
 * @param req
 * @param res
 */

/**
 * DELETE _one_ book
 * --------------------
 * Controller to delete _one_ books.
 *
 * Instructions, hints and questions
 * - Read about the 'findOne' method from Mongoose.
 * - Use the 'findOne' method from Mongoose.
 *   - Question: What is de result object from findOne?
 *   - Answer: one Book.
 *   - Question: What are the differences between MongoDb and Mongoose?
 *   Answer: MongoDb is database system . Mongoose is an object-relational mapping framework.
 * - The 'query' parameter is an empty object.
 *   - Question: Why is it empty?
 *   - Answer: it is not empty.
 * - Skip the options.
 * - Return all fields.
 * - Use the model "Book".
 * Question: Define route parameters and body parameter. What are the differences?
 * - Answer: Route parameters add additional info to the page: /books/'id'
 *           Body parameters tell you which page it is /books/'genre'
 * The return object has three properties:
 *
 * - meta: These are all optional and free to extend
 *   - method name: The name of the method
 *   - timestamp
 *   - filename: The name of the file. Use '__filename' for this.
 *   - duration: Duration of execution, time spend on server or other meaningful metric
 * - doc: The result object is either an object or null.
 * - err: If no errors, it has the value 'null'
 *
 * @module books/detail
 * @param req
 * @param res
 * @see http://docs.mongodb.org/manual/reference/method/db.collection.remove/
 * @see http://mongoosejs.com/docs/api.html#model_Model.delete
 */
/**
 * TODO: Create a DELETE document controller
 */

exports.deleteOne = function (req, res) {
    var conditions, callback, retObj;

    conditions = {_id: req.params._id};
    callback = function (err, doc) {
        retObj = {
            meta: {"action": "delete",
                'timestamp': new Date(),
                filename: __filename},
            doc: doc,
            err: err
        };
        return res.send(retObj);
    };

    Book.remove(conditions, callback);
};