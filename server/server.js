/*jslint node: true */
"use strict";
/** TODO: Test with static-analyzer: hints */
/** TODO: Test with static-analyzer: define module */

/**
 * Module dependencies.
 * @type {exports}
 */
/** TODO: Define variables */
var fs = require('fs'),
    http = require('http'),
    express = require('express'),
    bodyParser = require('body-parser'),
    env,
    config,
    mongoose,
    models_path,
    model_files,
    app,
    routes_path,
    route_files;

/**
 * Load configuration
 * @type {*|string}
 */
/** TODO: Load configuration */
env = process.env.NODE_ENV || 'development';
config = require('./config/config.js')[env];

/**
 * Bootstrap db connection
 * @type {exports}
 */
/** TODO: Define database connection */
mongoose = require('mongoose');
mongoose.connect(config.db);

/**
 * Debugging
 */
mongoose.connection.on('error', function (err) {
    console.error('MongoDB error: %s', err);
});
mongoose.set('debug', config.debug);

/**
 * Bootstrap models
 * @type {string}
 */
/** TODO: Read models */
models_path = __dirname + '/app/models';
model_files = fs.readdirSync(models_path);
model_files.forEach(function (file) {
    require(models_path + '/' + file);
});


/**
 * Use express
 * @type {*}
 */
/** TODO: Define express app */
app = express();
/**
 * Express settings
 */
/** TODO: Define port for express app */
app.set('port', process.env.PORT || config.port);

/**
 * Express middleware
 */
/** TODO: Add middleware to parse JSON input; @see https://github.com/expressjs/body-parser#bodyparserjsonoptions */
/** TODO: Add middleware to parse url-encoded input; @see https://github.com/expressjs/body-parser#bodyparserurlencodedoptions */
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

/**
 * Middleware to enable logging
 */
/** TODO: Add middleware to enable logging */
if (config.debug) {
    app.use(function (req, res, next) {
        console.log('%s %s %s', req.method, req.url, req.path);
        next();
    });
}

/**
 * Middleware to serve static page
 */
/** TODO: Define middleware to serve static pages */
app.use(express.static(__dirname + '/../client/'));

/**
 * Bootstrap routes
 * @type {string}
 */
/** TODO: Add routes */
routes_path = __dirname + '/routes';
route_files = fs.readdirSync(routes_path);
route_files.forEach(function (file) {
    var route = require(routes_path + '/' + file);
    app.use('/api', route);
});

/**
 * Middleware to catch all unmatched routes
 */
/** TODO: Add middleware to catch other requests */
app.all('*', function (req, res) {
    res.send(404);
});

/** TODO: Export app */
module.exports = app;