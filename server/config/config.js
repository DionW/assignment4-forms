/*jslint node:true*/
/** TODO: Test with static-analyzer */

module.exports = {
    development: {
        db: 'mongodb://localhost:27017/s505651',    // change books with your database
        port: 3000,                             // change 3000 with your port number
        debug: true                             // set debug to true|false
    },
    test: {
        db: 'mongodb://localhost:27017/s505651-test',   // change books with your database
        port: 3000,                             // change 1300 with your port number
        debug: false                            // set debug to true|false
    },
    production: {}
};