/*global angular, BookListCtrl, BookDetailCtrl */

/**
 * @see http://docs.angularjs.org/guide/concepts
 */
/** TODO: Initialize angular app */
var myApp = angular.module('myApp', ['myApp.services', 'ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        "use strict";
        // Get all books
        /** TODO: Create route for displaying all books */
        $routeProvider.when('/books', {
            templateUrl: 'partials/book-list.html',
            controller: BookListCtrl
        });

        $routeProvider.when('/books/:_id', {
            templateUrl: 'partials/book-detail.html',
            controller: BookDetailCtrl
        });

        $routeProvider.otherwise({
            redirectTo: "/books"
        });

    }]);